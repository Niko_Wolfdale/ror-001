# наследуемый класс позволяет работать с данными БД как с оычными классами Ruby
class Question < ApplicationRecord
  # validates создает проверки на соответствие определенным условиям
  validates :title, presence: true, length: {minimum: 3}
  validates :body, presence: true, length: {minimum: 3}

  def format_created_time
    self.created_at.strftime('%Y-%m-%d %H:%M')
  end
end
